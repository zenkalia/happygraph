require 'bundler'
require 'mandrill'
Bundler.require
require "forme/sinatra"
require 'erb'
include ERB::Util

DB = Sequel.connect(ENV['DATABASE_URL'])
set :database, DB

Sequel::Model.plugin :timestamps, :update_on_create => true
Sequel::Model.plugin :forme
require './models'

class Happygraph < Sinatra::Base
  enable :logging
  helpers Forme::Sinatra::ERB

  enable :sessions
  set :session_secret, ENV['SESSION_SECRET']

  helpers do
    def current_user
      @current_user ||= User[session[:user_id]]
    end

    def validate_auth!
      @token = AuthToken.where(token: params[:token]).first
      unless @token
        session.delete(:user_id)
        redirect '/'
      end
      @current_user = nil
      session[:user_id] = @token.user_id
    end

    def sanitize_params!
      sanitize_hash params
    end

    def sanitize_hash(hash)
      hash.each do |k,v|
        hash[k] = h(v) if v.class == String
        sanitize_hash v if v.class == Hash or v.class == Array
      end
    end
  end

  before do
    sanitize_params!
  end

  get '/' do
    redirect '/me' if current_user
    blogs = YAML.load_file './config/blogs.yml'
    haml :index, locals: { blogs: blogs }
  end

  get '/home' do
    'iono what you could do here, really'
  end

  post '/user' do
    user = User.where(email: params[:email]).first
    user ||= User.create(email: params[:email])
    redirect '/great'
  end

  get '/great' do
    haml :great
  end

  get '/checkin' do
    redirect '/' unless current_user
    haml :checkin_form, locals: {user: current_user, for_day: Date.today-1}
  end

  get '/checkin/:year/:month/:day' do
    for_day = Date.new(
      params[:year].to_i,
      params[:month].to_i,
      params[:day].to_i)
    redirect '/' unless session[:user_id]
    haml :checkin_form, locals: {user: current_user, for_day: for_day}
  end

  post '/checkin' do
    redirect '/' unless current_user
    checkin = Checkin.new
    checkin.user = current_user
    checkin.set_fields(params[:checkin], [:day, :happiness, :productivity, :happy_thing, :public])
    checkin.save
    redirect '/thanks'
  end

  get '/settings' do
    redirect '/' unless current_user

    haml :settings, locals: {user: current_user}
  end

  post '/settings' do
    redirect '/' unless current_user
    user_attrs = params[:user]
    current_user.update_fields(user_attrs, [:email, :email_time, :public, :sex, :sex_seeking, :birthday, :weekly_sleep, :weekly_drinks, :weekly_exercises, :weekly_naps, :weekly_work, :weekly_commute, :weekly_reading, :intro_extro, :diet])
    redirect '/settings'
  end

  get '/thanks' do
    redirect '/' unless current_user
    haml :thanks, locals: {posts: Checkin.where(public: true).exclude(happy_thing:'').last(5), user: current_user}
  end

  get '/me' do
    redirect '/' unless current_user
    haml :me, locals: {user: current_user, checkins: Checkin.where(user: current_user).order(Sequel.desc(:day))}
  end

  get '/random' do
    redirect '/' unless current_user
    haml :random, locals: {user: current_user, checkins: Checkin.where(public: true).exclude(happy_thing: '').all.sample(10)}
  end

  get '/login/:token' do
    validate_auth!
    redirect @token.url
  end

  get '/logout' do
    session.delete(:user_id)
    redirect '/'
  end

  get '/me/graphdata/:graph_type.json' do
    return 'lol no' unless current_user
    @user = current_user

    type = params[:graph_type]
    ret = {}

    if type == 'last_week'
      table = []
      table << ['Day', 'Happiness', 'Productivity']
      (-8..-2).each do |day_offset|
        happiness = 0.0
        productivity = 0.0
        count = 0
        for_day = Date.today + day_offset
        Checkin.where(user: @user, day: for_day).each do |checkin|
          happiness += checkin.happiness
          productivity += checkin.productivity
          count += 1
        end
        if count == 0
          table << [for_day, 0.1, 0]
        else
          table << [for_day, happiness/count + 0.05, productivity/count - 0.05]
        end
      end
      ret['table'] = table
      ret['title'] = 'Your Last Week'
    end
    ret.to_hash.to_json
  end

  get '/me/jollyplot.csv' do
    headers "Content-Disposition" => "attachment;filename=jollyplot.csv",
      "Content-Type" => "application/octet-stream"
    result = "day,happiness,productivity,happy_thing,created_at"
    Checkin.where(user: current_user).each do |checkin|
      result << "\n#{checkin.day},#{checkin.happiness},#{checkin.productivity},#{checkin.happy_thing},#{checkin.created_at}"
    end
    result
  end
end
