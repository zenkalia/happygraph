google.load('visualization', '1.0', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {

  $.getJSON('/me/graphdata/last_week.json', function(remote_data){
    // Create the data table.
    var data = google.visualization.arrayToDataTable(remote_data.table);

    // Set chart options
    var options = {title: remote_data.title, legend: {position: 'top' }, vAxis: {viewWindowMode: 'explicit', viewWindow: {min: 0, max: 5.1} } };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.LineChart(document.getElementById('my_week_chart_div'));
    chart.draw(data, options);
  })
}
