Sequel.migration do
  change do
    create_table(:auth_tokens) do
      primary_key :id
      foreign_key :user_id, :users
      String :token
      String :url
      DateTime :created_at, :null => false
      DateTime :updated_at, :null => false
    end
  end
end
