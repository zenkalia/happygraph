Sequel.migration do
  change do
    create_table(:checkins) do
      primary_key :id
      foreign_key :user_id
      Integer :happiness
      Integer :productivity
      String :happy_thing
      Boolean :public
      Date :day
      DateTime :created_at, :null => false
      DateTime :updated_at, :null => false
    end
  end
end
