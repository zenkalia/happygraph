Sequel.migration do
  change do
    add_column :users, :last_seen_ip, String
    add_column :users, :weekly_sleep, Integer
    add_column :users, :weekly_drinks, Integer
    add_column :users, :weekly_exercises, Integer
    add_column :users, :weekly_naps, Integer
    add_column :users, :weekly_work, Integer
    add_column :users, :weekly_commute, Integer
    add_column :users, :weekly_reading, Integer
    add_column :users, :intro_extro, String
    add_column :users, :diet, String
  end
end
