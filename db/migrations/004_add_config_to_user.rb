Sequel.migration do
  change do
    add_column :users, :email_time, String
    add_column :users, :sex, String
    add_column :users, :sex_seeking, String
    add_column :users, :birthday, Date
    drop_column :users, :email_verified
  end
end
