require 'haml'

class FakeHaml
  def self.render(email_template, locals)
    Haml::Engine.new(File.read(File.join('views', 'emails', "#{email_template.to_s}.haml"))).render(self, locals)
  end
end

class UserMailer
  def daily_email(user)
    today = Date.today - 1 # hack for the server being GMT
    date = today.strftime("%A %B, %-d")
    token = AuthToken.create(user:user, url:"/checkin/#{today.year}/#{today.month}/#{today.day}")
    m = Mandrill::API.new
    message = {
     :subject=> "JollyPlot - How was your day?",
     :from_name=> "JollyPlot.com",
     :to=>[
       {
         :email=> user.email,
         :name=> user.email
       }
     ],
     :html=>FakeHaml.render(:daily, {token: token, date: date}),
     :from_email=>"noreply@jollyplot.com",
     :track_opens => false,
     :track_clicks => false,
     :async => true
    }
    m.messages.send message
  end

  def welcome_back_email(user)
    today = Date.today - 1 # hack for the server being GMT
    date = today.strftime("%A %B, %-d")
    token = AuthToken.create(user:user, url:"/checkin/#{today.year}/#{today.month}/#{today.day}")
    m = Mandrill::API.new
    message = {
     :subject=> "JollyPlot - How was your day?",
     :from_name=> "JollyPlot.com",
     :to=>[
       {
         :email=> user.email,
         :name=> user.email
       }
     ],
     :html=>FakeHaml.render(:daily, {token: token, date: date}),
     :from_email=>"noreply@jollyplot.com",
     :track_opens => false,
     :track_clicks => false,
     :async => true
    }
    m.messages.send message
  end

  def welcome_email(user)
    today = Date.today - 1
    token = AuthToken.create(user:user, url:"/checkin/#{today.year}/#{today.month}/#{today.day}")
    m = Mandrill::API.new
    message = {
     :subject=> "JollyPlot - How was your day?",
     :from_name=> "JollyPlot.com",
     #:text=>"Hey this is your daily email, please tell us about your day!\n\n http://www.jollyplot.com#{token.auth_url}",
     :to=>[
       {
         :email=> user.email,
         :name=> user.email
       }
     ],
     :html=>"<p>Welcome to JollyPlot.  Thank you for joining.</p><p>There's only one blog post, but that's only because there's no data to analyze yet!  From now on you'll receive one email per day asking how you're doing.  How was your day?  We'd like to know.</p><p><a href='http://www.jollyplot.com#{token.auth_url}'> CLICK ME TO LOG IN</a></p><p>^__^</p>",
     :from_email=>"noreply@jollyplot.com",
     :track_opens => false,
     :track_clicks => false,
     :async => true
    }
    m.messages.send message
  end

  def blog_announce_email(user)
    blog = "http://blog.jollyplot.com/post/81364696231/jollyplots-exciting-new-future"
    token = AuthToken.create(user:user, url: blog)
    m = Mandrill::API.new
    message = {
     :subject=> "JollyPlot has been acquired!",
     :from_name=> "JollyPlot.com",
     #:text=>"Hey this is your daily email, please tell us about your day!\n\n http://www.jollyplot.com#{token.auth_url}",
     :to=>[
       {
         :email=> user.email,
         :name=> user.email
       }
     ],
     :html=>"<p>Hey Plotters.</p><p>JollyPlot's incredible journey is coming to an end.  We'd like to thank all of you for your participation.</p><p><a href='http://www.jollyplot.com#{token.auth_url}'>Click through to the blog for the full details.</a></p>",
     :from_email=>"noreply@jollyplot.com",
     :track_opens => false,
     :track_clicks => false,
     :async => true
    }
    m.messages.send message
  end
end

class User < Sequel::Model
  one_to_many :auth_tokens
  one_to_many :checkins

  def before_create
    self.email_time = '15'
    self.public = true
    super
  end

  def after_create
    usermailer = UserMailer.new
    usermailer.welcome_email(self) unless ENV['RACK_ENV'] == 'test' # this is dirty
    super
  end

  def streak
    checkins = Checkin.where(user: self).order_by(Sequel.desc(:created_at))
    streak = 0
    last_checkin = DateTime.now
    checkins.each do |checkin|
      if last_checkin.to_time.to_i - checkin.created_at.to_time.to_i < 36*60*60
        streak += 1
        last_checkin = checkin.created_at
      else
        break
      end
    end
    streak
  end

  def completeness
    demographic_fields = [:sex, :birthday, :sex_seeking, :weekly_sleep, :weekly_drinks, :weekly_exercises, :weekly_naps, :weekly_work, :weekly_commute, :weekly_reading, :intro_extro, :diet]
    complete_count = 0.0
    demographic_fields.each do |f|
      complete_count += 1 unless self.send(f).to_s == ''
    end
    (complete_count / demographic_fields.count * 100).to_i
  end

  def complete?
    completeness == 100
  end

  def self.email_times
    [ ['Never :(!', -1],
      ['5pm PST, 12am GMT', 0],
      ['6pm PST, 1am GMT', 1],
      ['7pm PST, 2am GMT', 2],
      ['8pm PST, 3am GMT', 3],
      ['9pm PST, 4am GMT', 4],
      ['10pm PST, 5am GMT', 5],
      ['11pm PST, 6am GMT', 6],
      ['12am PST, 7am GMT', 7],
      ['1am PST, 8am GMT', 8],
      ['2am PST, 9am GMT', 9],
      ['3am PST, 10am GMT', 10],
      ['4am PST, 11am GMT', 11],
      ['5am PST, 12pm GMT', 12],
      ['6am PST, 1pm GMT', 13],
      ['7am PST, 2pm GMT', 14],
      ['8am PST, 3pm GMT', 15],
      ['9am PST, 4pm GMT', 16],
      ['10am PST, 5pm GMT', 17],
      ['11am PST, 6pm GMT', 18],
      ['12pm PST, 7pm GMT', 19],
      ['1pm PST, 8pm GMT', 20],
      ['2pm PST, 9pm GMT', 21],
      ['3pm PST, 10pm GMT', 22],
      ['4pm PST, 11pm GMT', 23],
    ]
  end
end

class Checkin < Sequel::Model
  many_to_one :user

  def user_for_viewer(user)
    self.user == user ? 'You' : 'Anonymous'
  end

  def self.happiness_options
    [ ['1 - :(', 1],
      ['2 - :/', 2],
      ['3 - :|', 3],
      ['4 - :)', 4],
      ['5 - :D', 5] ]
  end

  def self.productivity_options
    [ ['1 - I got nothing done', 1],
      ['2 - Could have got more done', 2],
      ['3 - About normal', 3],
      ['4 - I got some things done', 4],
      ['5 - Super productive', 5] ]
  end
end

class AuthToken < Sequel::Model
  many_to_one :user

  def before_create
    self.token ||= SecureRandom.hex
    super
  end

  def auth_url
    "/login/#{self.token}"
  end

  def send_email!
    # TODO make this real
    puts "localhost:5000#{auth_url}"
  end
end

class EmailTime
  def self.times
    {
      '1' => '6:00pm PST / 2:00am GMT',
      #wuttt
    }
  end
end
