# Happygraph

Hey dudes let's collect numbers :)

## Running

```
bundle
cp .env.example .env
rake setup
foreman run shotgun
```

## Data?

```
foreman run rake console
```

## Emails?

You don't need a mandrill api token to dev on happygraph, just go
straight into the DB and pull out a token. Browse to `/login/[TOKEN]`
and you'll get logged in just right. Alternately, I think that you can
get the url from the email when it's puts'd to the console because of
sendmail? I don't quite remember.
