ENV['DATABASE_URL'] = 'postgres://localhost/happygraph_test'
ENV['RACK_ENV'] = 'test'

require_relative '../happygraph'
require 'rspec'
require 'rack/test'

RSpec.configure do |c|
  c.around(:each) do |example|
    DB.transaction(:rollback => :always){ example.run }
  end
end

describe Happygraph do
  include Rack::Test::Methods
  def app; Happygraph.new; end

  describe 'GET /' do
    subject { get '/' }
    its(:ok?) { should be }
  end

  describe User do
    let(:user){ User.create(email: 'whatever@cocks.com') }
    subject {user}

    it do
      subject.should be
    end

    describe '.streak' do
      subject { user.streak }

      describe 'with no checkins' do
        it do
          subject.should == 0
        end
      end
      describe 'with a few checkins' do
        before do
          Checkin.create(user: user, created_at: DateTime.now - 1)
          Checkin.create(user: user, created_at: DateTime.now - 2)
          Checkin.create(user: user, created_at: DateTime.now - 3)
        end
        it do
          subject.should == 3
        end
      end
      describe 'with a few checkins and a break' do
        before do
          Checkin.create(user: user, created_at: DateTime.now - 1)
          Checkin.create(user: user, created_at: DateTime.now - 2)
          Checkin.create(user: user, created_at: DateTime.now - 3)
          Checkin.create(user: user, created_at: DateTime.now - 5)
          Checkin.create(user: user, created_at: DateTime.now - 6)
        end
        it do
          subject.should == 3
        end
      end
    end

    describe '.completeness' do
      describe 'with no demo data' do
        its(:completeness){ should eq 0 }
        its(:complete?){ should_not be }
      end

      describe 'with full stuff' do
        before do
          user.sex = 'male'
          user.sex_seeking = 'female'
          user.birthday = Date.today
          user.weekly_sleep = 34
          user.weekly_drinks = 6
          user.weekly_exercises = 23
          user.weekly_naps = 2
          user.weekly_work = 8
          user.weekly_commute = 3
          user.weekly_reading = 3
          user.intro_extro = 'whatever'
          user.diet = 'chill'
          user.save
        end
        its(:completeness){ should eq 100 }
        its(:complete?){ should be }
      end
    end

  end
end
